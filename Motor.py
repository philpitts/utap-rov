# ============================================================================
# Motor Driver Software for UTAP Board
# ============================================================================

import RPi.GPIO as GPIO
from Adafruit_PWM_Servo_Driver import PWM

class MotorDriver:
    '''
    Software driver for controlling the UTAP board motor drivers
    '''
    # Wire name definitions
    # GR = Green, BL = Blue, OR = Orange, and BR = Brown.
    GR1 = "Green 1"
    GR2 = "Green 2"
    BL1 = "Blue 1"
    BL2 = "Blue 2"
    OR1 = "Orange 1"
    OR2 = "Orange 2"
    BR1 = "Brown 1"
    BR2 = "Brown 2"

    def __init__(self):
        '''
        Initialize the GPIO pins
        '''
        # --- GPIO Pins ---

        # The UTAP board motor drivers are controlled using the Raspberry Pi GPIO pins
        # GPIO stands for General Purpose Input / Output
        
        # When programming peripherals (like the motor drivers) you should first look at the microcontroller's pinout diagram
        # Open the "pinout-gpio.png" file in this folder to see the Raspberry Pi GPIO pinout diagram
        
        # You'll notice that the Raspberry Pi has 28 GPIO pins as well as 12 additional power and ground pins
        # Some of the GPIO pins can also be configured for other more advanced uses (UART, I2C, SPI)
        # The GPIO 0 and GPIO 1 pins generally should not be used except for advanced applications

        # There are two sets of IDs for the Raspberry Pi GPIO pins (yes - it's confusing and doesn't make much sense)
        # The first set of IDs are the GPIO IDs which can be seen in the pinout-gpio.png image
        # The second set of IDs are the BCM IDs which can be seen in the pinout-bcm.png image
        # The BCM IDs correspond to the pins on the Broadcom (a company) chip used by the Raspberry Pi
        # We will use the BCM IDs (the second set in pinout-bcm.png)

        # If you carefully follow the lines on the UTAP board, you may be able to see how the GPIO pins connect
        # Each of the GPIO pins will connect to the motor drivers (small green boards) and/or PWM controller (blue board)
        # More lines will come from the motor drivers and PWM controller and connect to the RJ45 (Ethernet) jacks

        # In most applications, you don't have to trace these lines, and will use a wiring diagram instead
        # Since this is a custom-made board, we know which GPIO pins connect to which board and for what purpose
        # Remembering which pins do what is hard for humans, so we'll store the pin numbers in an array
        # We will index this array according to the set of wire name definitions [GR1, GR2, BL1, ...] defined above
        # That way we can easily access the value we want using: self._direction_pin[GR1]

        # These pins control the direction each motor will spin
        # We will store the pin values in a dictionary container which will allow us to look up the pin value for a wire
        # Dictionaries have keys (in this case the wire names [e.g. GR1, GR2]) and values (in this case the pin numbers)
        # GR = Green, BL = Blue, OR = Orange, and BR = Brown.
        self._direction_pin = {
            MotorDriver.GR1: 19,
            MotorDriver.GR2: 21,
            MotorDriver.BL1: 13,
            MotorDriver.BL2: 17,
            MotorDriver.OR1: 4,
            MotorDriver.OR2: 20,
            MotorDriver.BR1: 27,
            MotorDriver.BR2: 26    # Can you find which pin is 26 in the pinout diagram?
        }

        # We will use the RPi.GPIO module to program the GPIO pins
        # https://sourceforge.net/p/raspberry-gpio-python/wiki/BasicUsage/

        # Set the GPIO ID configuration to BCM
        GPIO.setmode(GPIO.BCM)

        # Sometimes (for example if the program crashes) pins will remain configured
        # When this happens the GPIO module will print warnings telling us the pins are already configured
        # These warnings aren't useful for our application so we'll disable them
        GPIO.setwarnings(False)

        # Before we can use any of the GPIO pins, we need to tell the GPIO module how we plan to use them
        # In this case we want to write (not read) from all the GPIO pins so we'll set them in the OUT configuration
        # Remember that the pin numbers are the 'values' of the direction pin dictionary
        for pin in self._direction_pin.values():
            GPIO.setup(pin, GPIO.OUT)

        # --- PWM Pins ---

        # These pins control how fast each motor will spin
        # We could just turn them completly on and/or off (like a light switch) 
        # But it would be better if we could gradually turn them on/off (like a dimmer switch)

        # To gradually control the motor speeds we will use a technique called Pulse Width Modulation (PWM)
        # PWM works by cycling a pin on and off very quickly
        # The larger the pin's 'duty cycle' (or the percentage of time it is left on vs off) the faster the motor will turn
        
        # Think of it like pushing a cart in the grocery store: if you use a finger to push the cart with the same force
        # 0.3s (30% of a second), it will go faster than if you only pushed it (with the same force) 0.1s (10% of second)

        # These pins are *NOT* GPIO pins
        # These pins are actually the PWM pins on the blue board
        # We configure these pins by communicating with the blue board over the I2C (Inter-Integrated Circuit) protocol
        # I2C is an advanced topic: https://learn.sparkfun.com/tutorials/i2c/all
    
        # PWM Pin IDs (not GPIO!)
        # GR = Green, BL = Blue, OR = Orange, and BR = Brown.
        self._pwm_pins = {
            MotorDriver.GR1: 1,
            MotorDriver.GR2: 5,
            MotorDriver.BL1: 3,
            MotorDriver.BL2: 7,
            MotorDriver.OR1: 0,
            MotorDriver.OR2: 4,
            MotorDriver.BR1: 2,
            MotorDriver.BR2: 6
        }

        # Configure the PWM frequency
        # The PWM frequency is the amount of time it takes to complete one cycle
        # Remember that the 'duty cycle' (defined above) is percent of time the pin will be on during one cycle
        # This 'frequency' controls how often a cycle will be performed
        
        # To continue the grocery cart analogy from above, our cart pushing cycle frequency is one second (1Hz)
        # If we were to define our cycle as 0.5s we would complete two cycles per second (2Hz)
        # Pushing 0.2s (10% of a second for 2 cycles) will make the cart go faster than pushing 0.1s (10% of a 
        # second for 1 cycle)

        # The amount of power generated on the PWM pin is related to both the frequency and duty cycle!

        # In most cases, the frequency is set at a constant and the duty cycle is dynamically varied
        self._pwm_board = PWM()
        self._pwm_board.setPWMFreq(1600)    # 1.6kH (1600 cycles per second)

    def SetMotor(self, wire, value):
        '''
        Sets the speed and direction of a motor

        :param wire str: A predefined wire name (e.g. MotorDriver.GR1)
        :param value float: A value between [-1..1] to assign to the motor
        '''
        # Make sure the wire is valid by looking for it in one of our dictionaries
        if self._direction_pin.get(wire) == None:
            print "%s is not a valid wire name. Use the defined wire names (e.g. MotorDriver.GR1)" % wire
        # Make sure the value is between [-1..1]
        if value < -1 or value > 1:
            print "Invalid motor value: %s. Motor values should be between [-1..1]" % value

        # Set the direction of the motor
        if value < 0:
            GPIO.output(self._direction_pin[wire], GPIO.LOW)
        else:
            GPIO.output(self._direction_pin[wire])

        # Set the speed of the motor
        # The motor driver module expects the motor speed value to be between [0, 4096)
        # Convert the magnitude of our value (the speed) to be between [0, 4096)

        # Note that we are changing the PWM duty cycle here (see above) not the frequency
        # In this case a (nominal) 100% duty cycle will be 4095 and a 0% duty cycle will be 0
        value = abs(value) * 4095
        self._pwm_board.setPWM(self._pwm_pins[wire], 1, value)