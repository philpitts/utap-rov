# ============================================================================
# Tutorial: Level 1
# ============================================================================

# Welcome!
# In this tutorial we will setup our first python file and get our ROV moving forward

# 1. Comments
# -----------------------------------------------------------------------------
# - Anything with '#' in front of it is known as a comment
# - Comments don't effect the python program, but they are useful for documenting your code
# - As you can see, this tutorial is written using comments
# -----------------------------------------------------------------------------

# 2. Modules
# -----------------------------------------------------------------------------
# - To keep things organized, we put related pieces of code in their own files
# - In python, these files are known as 'modules'
# - To use code located in another module we must first import it
# - Let's do this now to import MotorDriver from the motor module
# -----------------------------------------------------------------------------
from Motor import MotorDriver

# 2. Functions
# -----------------------------------------------------------------------------
# - A 'function' is set of instructions that accomplish one task
# - Functions are a bit like cooking recipies
#
# - For example to make a cake, one might:
#       a. Measure out the ingredients
#       b. Sift together the flour and baking powder
#       c. Beat the eggs and sugar together
#       d. Mix the dough together
#       e. Bake the dough
#
# - Functions often call other functions
# - For example, in order to measure the ingredients we might call the measure() function
# - The Measure() function in turn will have a set of instructions which may also call additional functions
#
# - Functions can have parameters
# - For example, the measure() function might have a parameter which describes what is supposed to be measured
# - In python, parameters are passed to a function inside parentheses '()', for example:
#       a. measure(ingredient)
#       b. measure(flour)
#       c. measure(salt)
#
# - Multiple function parameters are seperated by a comma (,)
#       a. sift(flour, salt)
#
# - Functions can also have return values
# - For example, our measure() function might return how much of the ingredient we have
#       a. cups_flour = measure(flour)
#       b. cups_salt = measure(salt)
#
# - Functions are always called with (), even if they don't have parameters
#       a. say_hello()
#       b. bake_cake()
#
# - To define a function, use the 'def' keyword
# - Following the def keyword write the function name
# - Following the function name put the paramters of the function in parentheses
# - Leave the parentheses blank if there are no parameters
#       a. def measure(ingredient):
#       b. def bake_cake():
#
# - You may have noticed the ':' at the end of each of the function definition examples
# - The ':' character defines the start of a block of code
# - In this case that block of code will hold the definition of our function
#       a. def bake_cake():
#              measure(flour)
#              sift_dry_ingredients()
#              ...
#
# - Note that the code block is indented
# - As long as the code remains indented, it is considered inside the code block
# - If the indentation changes, the code is considered to be in another block
#
# **************************
# INDENTATION IS IMPORTANT
# **************************
# - Make sure you use consistent indentation
# - Most errors experienced by new python coders are indentation related!!!
#
# - Let's create a function to hold the logic for moving the ROV forward
# - We'll call it 'main' since it is the main function of our program
# -----------------------------------------------------------------------------
def main():
    # 3. Variables
    # -------------------------------------------------------------------------
    # - Variables are values which can be assigned, modified, and used
    # - Programming variables work just like math variables
    #       a. x = 42                       # <-- x is a variable
    #       b. y = 7                        # <-- y is a variable
    #       c. pi = 3.14159265359           # <-- pi is a variable
    #       d. my_super_cool_variable = 3   # <-- my_super_cool_variable is a variable
    #
    # - Variables don't have to just be numbers
    #       a. name = "Bob"                 # <-- name is a variable
    #       b. my_cool_car = build_car()    # <-- my_cool_car is a variable
    #
    # Let's assign a variable to hold the motor driver
    # -------------------------------------------------------------------------
    driver = MotorDriver()

    # 4. Objects
    # -------------------------------------------------------------------------
    # - An object is how we represent a thing (or a noun) in python (and many other programming languages)
    # - Objects can have variables and functions associated with them that describe it and make it do things
    #
    # - For example we might have a 'Cat' object
    # - We can assign a Person variable just as we did before
    #       a. cali = Cat()
    #
    # - Our cat may have attributes associated with them
    # - These object variables (or attributes) are called 'member variables'
    #       a. cali.name = "Cali the Cat"
    #       b. cali.age = 2
    #
    # - Our cat may also be able to perform actions
    # - These actions are called 'methods' and are simply functions defined in an object
    # - We can call object methods using the variable the object is assigned to
    #       a. cali.meow()
    #       b. cali.purr()
    #
    # - Most objects have a function which creates a new 'instance' or copy of the object
    # - This function is called a constructor as is called when assigning a new variable
    # - You may have noticed that we used () when creating Cali
    # - We were actually calling the Cat constructor to get a new Cat instance
    # -------------------------------------------------------------------------

    # 5. Challenge
    # -------------------------------------------------------------------------
    # - The variable we just created, 'driver', is a MotorDriver object
    # - The MotorDriver object has a method, SetMotor(), that we can use to assign motor values
    #
    # - The SetMotor(id, value) method takes two parameters:
    #       a. The ID of the motor to activate
    #       b. The value to assign to the motor
    #
    # - The motor ID can be any of the following values:
    #       a. MotorDriver.BL1      # <-- Motors connected to the blue wires
    #       b. MotorDriver.BL2
    #       b. MotorDriver.GR1      # <-- Motors connected to the green wires
    #       b. MotorDriver.GR2
    #       b. MotorDriver.OR1      # <-- Motors connected to the orange wires
    #       b. MotorDriver.OR2
    #       b. MotorDriver.BR1      # <-- Motors connected to the brown wires
    #       b. MotorDriver.BR2
    # 
    # - The value to assign to the motor can be any number between [-1..1]
    #
    # - Turn on a motor using the 'SetMotor()' method of the 'MotorDriver' object stored in 'driver'
    # - Once you have one motor working, try turning on multiple motors (hint: call SetMotor() multiple times)
    # - Can you get your ROV to swim forward?
    # -------------------------------------------------------------------------
    

# * Boilerplate
# -----------------------------------------------------------------------------
# This is standard python boilerplate which will call the main() function when the program is started
# -----------------------------------------------------------------------------
if __name__ == "__main__":
    main()