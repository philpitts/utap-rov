# UTAP ROV
## Undersea Technology Apprentice Program (UTAP) Remotely Operated Vehicle (ROV)

Software to operate the Undersea Technology Apprentice Program (UTAP) Remotely Operated Vehicle (ROV). Based on original work by Dr. John DiCecco of NUWC Division Newport.

Additional instructions under development (see python files for now).