# ============================================================================
# LED Driver Software for UTAP Board
# ============================================================================

from time import sleep
import RPi.GPIO as GPIO
from Adafruit_PWM_Servo_Driver import PWM

# This driver is very similar to the motor driver implementation
# Review 'Motor.py' for a tutorial on the programming concepts in this module

class LEDDriver:
    '''
    Software driver for controlling the LEDs on the UTAP board
    '''

    def __init__(self):
        '''
        Initializes the GPIO pins
        '''
        
        # This list contains all the GPIO pin numbers that control the bank of LEDs on the daughter
        # board. They are ordered so that the first blue LED(D1) is first and the last green LED(D9)
        # is last.

        # The board has the names of each LED (D1, D2, ...) printed in white near the component
        self.pins = [23, 24, 18, 22, 25, 12, 5, 6, 16]

        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)

        # Iterate through each of the LED pins and configure them for writing (OUT)
        for x in range(0, len(self.pins)):
            GPIO.setup(self.pins[x], GPIO.OUT)

    def set_pin(self, pin, value):
        '''
        Sets the value of an LED

        :param pin int: The GPIO pin of the LED to set
        :param value int: Either GPIO.HIGH or GPIO.LOW
        '''
        GPIO.output(pin, value)


    def set_all(self, pin_array, value):
        '''
        Set all pins in a given array to the given value

        :param pin_array array: Array of LED GPIO pins
        :param value int: Either GPIO.HIGH or GPIO.LOW
        '''
        for pin in pin_array:
            self.set_pin(pin, value)

    def pin_run(self, pin_array, duration):
        '''
        Sequentially set each pin in the given array high for the given duration

        :param pin_array array: Array of LED GPIO pins
        :param duration float: The length of time (in seconds) to pause between changing LEDs
        '''
        for pin in pin_array:
            self.set_pin(pin, GPIO.HIGH)
            # The sleep() function pauses execution for the specified number of seconds
            sleep(duration)
            self.set_pin(pin, GPIO.LOW)

    def led_test_pattern(self):
        '''
        Displays a predictable pattern on the LEDs to aid in troubleshooting
        '''
        self.pin_run(self.pins, 0.1)
        self.pin_run(list(reversed(self.pins))[1:], 0.1)
        sleep(0.2)
        self.set_all(self.pins, GPIO.HIGH)
        sleep(0.2)
        self.set_all(self.pins, GPIO.LOW)
        sleep(0.2)
        self.set_all(self.pins, GPIO.HIGH)
        sleep(0.2)
        self.set_all(self.pins, GPIO.LOW)
        sleep(0.2)