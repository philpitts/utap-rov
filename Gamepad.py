# ============================================================================
# Logitech Gamepad F310 Driver
#
# Uses the Linux Kernel Joystick API
# https://www.kernel.org/doc/Documentation/input/joystick-api.txt
# ============================================================================

import struct
import os
import array
from fcntl import ioctl

class LogitechF310Gamepad:
    '''
    Software driver for reading input from the Logitech F310 Gamepad
    '''

    # These constants are used to read values for each of the different gamepad axes
    # The way to find these values is documented in: https://www.kernel.org/doc/Documentation/input/joystick-api.txt
    # Think of these numbers as IDs for each of the different axes and buttons
    _axis_names = {
        0x00: 'x',
        0x01: 'y',
        0x02: 'rx',
        0x03: 'x2',
        0x04: 'y2',
        0x05: 'ry',
        0x10: 'hat0x',
        0x11: 'hat0y',
    }

    _button_names = {
        0x130: 'a',
        0x131: 'b',
        0x133: 'x',
        0x134: 'y',
        0x136: 'LB',
        0x137: 'RB',
        0x13a: 'select',
        0x13b: 'start',
        0x13c: 'mode',
        0x13d: 'thumbl',
        0x13e: 'thumbr',
    }


    def __init__(self, gamepad_number = 0):
        '''
        Initialize the gamepad

        :param int device_number: The index of the gamepad to initialize (in case multiple gamepads are connected)
        '''
        # --- Open the joystick device ---

        # All devices, including gamepads and joysticks, are represented in Linux in the '/dev' file directory
        # In our case, we know that gamepads and joysticks will be located in the '/dev/input' file directory
        # To see all the devices for yourself try typing 'ls /dev' and 'ls /dev/input' in the terminal
        
        # When your gamepad is plugged in, you should see a file called 'js0'.
        # If you plug two gamepads in you should see two files called 'js0' and 'js1'.
        # Note: Linux calls gamepads 'joysticks'. The 'js' stands for 'joystick'.

        # To make things easier, we've specified a parameter (gamepad_number) that lets us select which gamepad to use.
        # We can dynamically create the full filename of the gamepad by joining or 'concatenating' two strings together.
        # Note: A string is a series of characters (e.g. 'hello world' or 'I am another string' or 'string #3 - whoo!')
        joystick_device_filename = '/dev/input/js' + str(gamepad_number)

        # To help solve problems (or 'debug' a program) it can be useful to print periodic status messages
        # Use the 'print' function to print messages to the command line
        # To make printing strings easier, the '%' operator can be used to substitute in parts of the string
        
        # For example: print 'I am %s the super %s string' % ('Sally', 'cool')
        # Would resolve to: 'I am Sally the super cool string'
        print ('Opening %s...' % joystick_device_filename)

        # Use the 'open' function to open a file
        # The 'rb' parameter tell the function to open the binary ('b') file for reading ('r')
        self._joystick_device = open(joystick_device_filename, 'rb')

        # --- Get the device name ---
        
        # Initialize a 64-byte array to save the device name in
        # This looks intimidating but just creates a table (or array) with the following
        
        # Index: | 0 | 1 | 2 | ... | 63 |
        # Value: | 0 | 0 | 0 | ... | 0  |

        # The first parameter, 'c', tells the function to create characters ('a', 'b', 'c', ...) 
        # Other choices might include integer numbers (0, 1, 2, ...) or decimals (1.1, 1.2, 1.3, ...)
        
        # Each character uses 1 byte of memory. One byte is 8 bits. A bit is a fancy word for an 'on/off' switch.
        # A bit can either be 'on' (1) or 'off' (0). That means that one bit can store 2 values (0 and 1).
        # 2 bits can store 2 sets of 2 values. That means it can store 4 total values (00, 01, 10, 11)
        # 3 bits can store 2^3 (2 cubed) values. 2^3 = 8 values: 000, 001, 010, 011, 100, 101, 110, 111
        # 8 bits (or a byte) can store 2^8 = 256 values.
        # Fun fact: 4 bits is equal to 1 'nibble' (get it?)
        
        # 256 values is enough to store most common characters. These common characters are called 'ASCII' characters.
        # Google 'ASCII table' to see what they are.

        # The second parameter specifies that we want the array to have 64 entries
        # Each of those entries should be the '\0' (null) character which has a value of 0 (look at the ASCII table!)
        # Note: The character '0' (zero) is 48 according to the ASCII table

        # Note that we use array.array to specify the python module and the function to call inside that module
        # The python module is called 'array', the function inside that module we are calling is also called 'array'
        input_buffer = array.array('c', ['\0'] * 64)

        # Read the device name into the input_buffer (advanced topic)
        # This function call (ioctol) reads a set of characters in a particular way from the binary file we opened

        # The 'particular way' to read the characters (opcode) is defined in a system dependent C library
        # The hexadecimal numbers used are not documented.
        # These numbers can can be determined by examining the Raspberry Pi implementation of joystick.h
        # https://github.com/torvalds/linux/blob/master/include/uapi/linux/joystick.h
        
        # Hexadecimal numbers can be recognized by the '0x' prefix
        # Each hexadecimal digit can represent 16 values (just as each binary digit can represent 2)
        # That means that 2 hexadecimal digits can represent 16^2 = 256 values (we've seen that number before!)
        # That means a byte can be represented by 2 hexadecimal digits (e.g. 0x00 is '0')
        # This is a very compact way to view bytes and you'll often see them represented this way!
        # Note that since each digit can have 16 values we use 'a-f' to represent '10-15'.

        # We say that binary is 'base-2' because the base of the power expression is '2'.
        # Similarly, hexadecimal is 'base-16' because the base of the power expression is '16'.
        # Normal numbers are 'base-10' because there are 10 values that can be represented as single digit numbers

        # The first parameter (self.joystick_device) specifies which file to read the characters from
        # The second parameter is the opcode we discussed earlier
        # The third parameter is the array we created earlier to store the values
        ioctl(self._joystick_device, 0x80006a13 + (0x10000 * len(input_buffer)), input_buffer)  # JSIOCGNAME(len)

        # --- Get number of axes ---
        
        # Use 'B' this time to specify an array of unsigned integers with 1 byte of storage
        # Since 1 byte equals 8 bits and 8 bits can store 2^8 = 256 values, this can hold values of [0, 255]
        # In this case, our array will only have a single entry with value '0'
        input_buffer = array.array('B', [0])
        ioctl(self._joystick_device, 0x80016a11, input_buffer)  # JSIOCGAXES

        # Our input buffer only has one entry so we know the value we want is in index 0
        # Why even have an array if there's only one value? Because it's what the ioctl function expects.
        num_axes = input_buffer[0]

        # --- Get the axis map ---

        # How many entries in this one? What does the hexadecimal number 0x40 translate into (in base-10)?
        # Convert by multiplying the first digit by 16 and then adding the second digit: 4 * 16 + 0 = 64
        input_buffer = array.array('B', [0] * 0x40)
        ioctl(self._joystick_device, 0x80406a32, input_buffer)  # JSIOCGAXMAP

        # Create containers to store the axis IDs and joystick positions
        self._axis_map = []
        self._axis_states = {}

        # Iterate through the num_axes entries in the input_buffer in order: [0..num_axis)
        for axis in input_buffer[:num_axes]:
            # Try and find the axis in the system file
            # If the axis can't be found save it as 'unknown' so we know something went wrong
            axis_name = self._axis_names.get(axis, 'unknown(0x%02x)' % axis)

            self._axis_map.append(axis_name)
            self._axis_states[axis_name] = 0.0

        # Print out all the mapped axes
        # The join function can be used to quickly create a string from all the values in a container
        print '%d axes found: %s' % (num_axes, ', '.join(self._axis_map))

        # --- Get number of buttons ---
        input_buffer = array.array('B', [0])
        ioctl(self._joystick_device, 0x80016a12, input_buffer) # JSIOCGBUTTONS
        num_buttons = input_buffer[0]

        # --- Get the button map ---
        # Use 'H' to specify an array of unsigned integers with 2 bytes of storage
        # That means 2^16 = 65536 possible values in the range [0, 65535]
        input_buffer = array.array('H', [0] * 200)
        ioctl(self._joystick_device, 0x80406a34, input_buffer)  # JSIOCGBTNMAP

        self._button_map = []
        self._button_states = {}

        for button in input_buffer[:num_buttons]:
            btn_name = self._button_names.get(button, 'unkown(0x%03x)' % button)
            self._button_map.append(btn_name)
            self._button_states[btn_name] = 0

        print '%d buttons found: %s' % (num_buttons, ', '.join(self._button_map))

        # --- Initialize the public axis variables (used outside the class) ---
        self.right_stick_y = 0
        self.right_stick_x = 0
        self.left_stick_y = 0
        self.left_stick_x = 0
        self.right_trigger = 0
        self.left_trigger = 0

    @staticmethod
    def enumerate_joystick_devices():
        '''
        Print out all the available joystick devices
        '''
        # This method is a static method which means it can be called without creating an instance of the class

        print 'Available devices:'

        # This loop iterates through each of the files in the path passed to listdir()
        for filename in os.listdir('/dev/input'):
            # Only print out the files that start with 'js'
            print '  /dev/input/%s' % filename

    def update(self):
        '''
        Checks for new gamepad input and updates the stored values
        '''
        # Poll the joystick file for an update
        # To do this, we will need to read 8 bytes from the joystick device file
        # We know to read 8 bytes because the system documentation provides a description of the data we are going to read
        # https://www.kernel.org/doc/Documentation/input/joystick-api.txt
        
        # Event Structure:
        # ---------------------------------------
        # | Name       | Type             | Bytes
        # | Timestamp  | unsigned integer | 4 
        # | Value      | signed integer   | 2
        # | Event Type | unsigned integer | 1
        # | ID         | unsigned integer | 1   (see _axis_names and _button_names at top)
        # ---------------------------------------
        # Total: 8 bytes
        event = self._joystick_device.read(8)

        # Check to make sure we actually got an update (sometimes we might not)
        if event:
            # Unpack the structure from above
            # The 'IhBB' string represents the types from the table, in order (starting with: unsigned integer - 4 bytes)
            timestamp, value, event_type, id_number = struct.unpack('IhBB', event)

            # Next we'll handle the different cases of the event.
            # The event can either be a 'startup event', an 'axis event', or a 'button event'
            # The codes for each of these events are documented in section 2.1 of:
            # https://www.kernel.org/doc/Documentation/input/joystick-api.txt

            # Notice that we use '&' instead of '=='
            # This is because the startup event can happen at the same time as a button or axis event

            # If we got both a startup event and axis event then the event_type would be 0x81
            # If we got both a startup event and button event then the event_type would be 0x82

            # If we used '==' we wouldn't detect either event since 0x80 != 0x81 != 0x82

            # Let's checkout what this looks like in binary
            # -------------------------------------
            # 0x01: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1
            # 0x02: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0
            # 0x80: 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0
            # -------------------------------------
            # Notice how each number has it's own bit?

            # The '&' operator is called a 'bitwise and'
            # When comparing two binary numbers, in each digit it will only write a '1' if both digits are '1'
            # Example: 0x42 (0 0 0 0 0 1 0 0 0 0 0 0 0 0 1 0) &
            #          0x71 (0 0 0 0 0 1 1 1 0 0 0 0 0 0 0 1)
            #        = 0x40 (0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0)

            # You can see that if we used the '&' operator with 0x01, 0x02, or 0x80 we would always get '0x00' (zero)
            # However, let's try it with a couple of the combined event type numbers:
            # -------------------------------------
            #   0x81 (0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 1) &
            #   0x01 (0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 1)
            # = 0x01 (0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 1) <-- not zero
            #
            #   0x81 (0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 1) &
            #   0x80 (0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0)
            # = 0x80 (0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0) < -- not zero

            # If the result of our '&' operation is not zero, that means our event must have occured!

            # Bit operations like '&' are fast ways to check if a bit is set
            # Storing multiple values in a single byte (as done here) also helps save memory
            # Imagine if we had 32 values instead of just 3
            # Storing each of the 32 values in a seperate byte would cost 32 bytes (when it could just cost 1)

            # Handle startup event
            if event_type & 0x80:
                print "(initial)"

            # Handle button press event
            if event_type & 0x01:
                # Get the button name from the button ID
                button = self._button_map[id_number]
                # Check to make sure there actually is a name for this ID (in case we forgot to map one)
                if button:
                    self._button_states[button] = value

            # Handle axis event
            if event_type & 0x02:
                axis = self._axis_map[id_number]

                # Normalize the axis value to a number between [-1..1]
                decimal_value = self._normalize(value)
                self._axis_states[axis] = decimal_value

                # Right joystick forward/back
                if axis == "y2":
                    self.right_stick_y = decimal_value
                # Right joystick left/right
                if axis == "x2":
                    self.right_stick_x = decimal_value
                # Left joystick forward/back
                if axis == "y":
                    self.left_stick_y = decimal_value
                # Left joystick left/right
                if axis == "x":
                    self.left_stick_x = decimal_value
                # Right trigger
                if axis == "ry":
                    self.right_trigger = decimal_value
                # Left trigger
                if axis == "rx":
                    self.left_trigger = decimal_value

    def _normalize(self, value):
        '''
        Normalize a gamepad axis value to between [-1..1]

        :param int value: The gamepad axis value to normalize
        :return float: The normalized decimal value between [-1..1]
        '''
        # Signed integers only use one value slot to store 0
        # That means that 16-bit (2 byte) integers can have values from [-2^15..2^15-1] or [-32,768..32,767]
        
        # If we get a number between [-32767..32767] we can divide it by 32767 to get a number between [-1..1]
        # When we divide we want to be sure make the divisor a decimal to avoid accidental integer divison
        # We'll also explicitly check for -32768 and set that to -1

        # Interested in how the sign (+/-) is stored? Check out Two's Complement:
        # https://stackoverflow.com/questions/1049722/what-is-2s-complement

        if value == -32768:
            return -1
        return value / 32767.0